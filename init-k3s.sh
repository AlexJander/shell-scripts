#!/bin/bash

# Verificar se o K3s está instalado
if ! command -v k3s &> /dev/null; then
    echo "K3s não está instalado. Instalando..."
    # /path/to/k3s start
    sudo curl -Lo /usr/local/bin/k3s https://github.com/k3s-io/k3s/releases/download/v1.26.5+k3s1/k3s
    # Permissão de execução
    sudo chmod a+x /usr/local/bin/k3s
else
    # Caso o K3s esteja instalado, retorne a versão
    echo "O K3s já está instalado e funcionando!!!"
    echo "----------------------------------------"
    k3s --version
    echo "----------------------------------------"
fi
