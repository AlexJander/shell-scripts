#!/bin/bash

echo "---------------------------------------------------------------------"
# Adicionando o repositório do PostgreSQL
sudo sh -c 'echo "deb https://apt.postgresql.org/pub/repos/apt $(lsb_release -cs)-pgdg main" > /etc/apt/sources.list.d/pgdg.list'
wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -
sudo apt-get update

echo "---------------------------------------------------------------------"
# Instalando o PostgreSQL 12 (ou a versão desejada)
sudo apt-get -y install postgresql-12
echo "---------------------------------------------------------------------"

# Instalando o Nginx
sudo apt update
sudo apt -y install nginx
echo "---------------------------------------------------------------------"

# Verificar versões
echo "---------------------------"
echo "| INSTALAÇÃO CONCLUÍDA!!! |"
echo "---------------------------"
echo "---------------------------------------------------------------------"
echo "| As versões a seguir foram instaladas:                             | "
echo "---------------------------------------------------------------------"
nginx -v
pg_config --version

