#!/bin/bash

# Cria o diretório para instalar o Miniconda
mkdir -p ~/miniconda3

# Baixa o instalador do Miniconda
wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh -O ~/miniconda3/miniconda.sh

# Instala o Miniconda
bash ~/miniconda3/miniconda.sh -b -u -p ~/miniconda3

# Remove o instalador após a instalação
rm -rf ~/miniconda3/miniconda.sh

# Inicializa o Miniconda para o Bash
~/miniconda3/bin/conda init bash

# Inicializa o Miniconda para o Zsh
~/miniconda3/bin/conda init zsh
